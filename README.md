# Typescript index.ts generator

Automatically generates `index.ts` files re-exporting everything from all the files in the same folder.  
It does not create an index file in the root directory itself.

## Usage

`generate-index-ts <root-directory> [--watch]`