#!/usr/bin/env node

let args = process.argv.slice(2);

if (args.length < 1) { 
    console.log("usage: generate-index-ts <root directory> [--watch] [--silent]");
    process.exit(2);
}

let hasOption = option => args.slice(1).indexOf(option) >= 0;

let generateIndexTs = require('./index.js');
let rootDirectory = args[0];
let options = {
    watch: hasOption('--watch'),
    silent: hasOption('--silent')
};

let watcher = generateIndexTs(rootDirectory, options);

process.on('SIGINT', () => watcher.close());